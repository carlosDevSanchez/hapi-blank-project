'use strict'

const config = require('./config');
const routes = require('./lib/routes');
const db = require('./lib/model');
const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const { Users } = require('./lib/model');

const swaggerOptions = {
  info: {
    title: "Nombre api",
    description: "Documentacion proyecto Nombre"
  },
  schemes: ['https', 'http'],
  securityDefinitions: {
    jwt: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header'
    }
  },
  security: [{ jwt: [] }]
};

const start = async () => {
  /**
   * configuramos los aspectos principales, como el puerto el host y habilitamos las cors.
   */
  const server = Hapi.server({
    port: config.APP_PORT,
    host: config.APP_HOST,
    routes: { cors: { origin: ['*'], additionalHeaders: ['cache-control', 'x-requested-with'] }}
  });

  /**
   * registramos nuestra app con autenticacion
   */
  await server.register([
    require('hapi-auth-jwt2'),
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
    // {
    //   //plugin: require('hapi-cron'), // aqui se activan la cron jobs
    //   //options: require('./lib/cron')
    // }
  ]);

  server.auth.strategy('jwt', 'jwt', {
    key: config.KEY,
    validate: async decoded => {
      const user = await Users.findOne({ _id: decoded.id });
      if (!user) { //validar el usuario
        return { isValid: false };
      } else {
        return { isValid: true };
      }
    },
    verifyOptions: { algorithms: ['HS256'] }
  });

  server.auth.default('jwt');
  server.route(routes);

  await server.start();
  await db.connect();
  console.log(`Server running at: ${server.info.uri}`);
  return server;
}

start();