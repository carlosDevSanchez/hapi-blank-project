const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    code: {
      type: String,
    },
    firts_name:{
      type: String,
    },
    last_name: {
      type: String
    },
    email: {
      type: String
    },
    phone: {
      type: String
    },
    image: {
      type: String
    },
  },{
    discriminatorKey: 'kind'
});

module.exports = mongoose.model('Users', userSchema);
