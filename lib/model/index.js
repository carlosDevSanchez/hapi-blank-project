const config = require('../../config')
const mongoose = require('mongoose')

module.exports = {
  connect: async () => {
    mongoose.set('debug', true);
    await mongoose.connect(
      config.DB_HOST + '/' + config.DB_NAME,
      config.DB_OPTS)
  },
  connection: mongoose.connection,
  Users: require('./Users'),
  Admin: require('./Admin'),
}
