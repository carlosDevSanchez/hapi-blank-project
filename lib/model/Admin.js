const mongoose = require('mongoose');
const Users = require('./Users');
const bcryptjs = require('bcryptjs');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    password: {
        type: String,
        required: true
    },
});

userSchema.pre('save', function(next){
  const self = this;
  if (!self.isModified('password')) return next();

  bcryptjs.genSalt(12, (err, salt) => {
      if (err) return next(err);
      bcryptjs.hash(self.password, salt, (err, hash) => {
          if (err) return next(err);
          self.password = hash;
          next();
      });
  });
})

userSchema.pre('findOneAndUpdate', function(next){
  const self = this;
  if (self._update.password && self._update.password !== ''){
    bcryptjs.genSalt(12, (err, salt) => {
      if (err) return next(err);
      bcryptjs.hash(self._update.password, salt, (err, hash) => {
          if (err) return next(err);
          self._update.password = hash;
          next();
      });
    });
  }else{
    next();
  }
})

module.exports = Users.discriminator('Admin', userSchema);
