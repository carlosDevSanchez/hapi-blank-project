//esto sirve para generar tareas automaticas, en cierto horario correrá algun punto de la api para ejecutar tareas...
const cron = {
    jobs: [
        {
            name: 'Name',
            time: '0 0 18 * * *',
            timezone: 'America/New_York',
            request: {
                method: 'POST',
                url: '/v1/name'
            },
            onComplete: (res) => {
                console.log(res);
            }
        },
    ]
};

module.exports = cron;