'use strict';

const Boom = require('boom');
const uploadS3 = require('../utils/file-upload');

module.exports = [
    {
        path: '/v1/upload',
        method: 'POST',
        options: {
            handler: async (request, h) => {
                const { payload: { file } } = request;

                const data = await uploadS3(file);
                if(!data){
                    Boom.forbidden();
                }

                return {
                    data,
                    status: 'SUCCESS'
                }
            },
            description: 'Subida de archivos a aws s3',
            tags: ['api', 'upload', 'file'],
            payload: {
                output: 'stream',
                multipart: true,
                parse: true,
                maxBytes: 1000 * 1000 * 1000,
                allow: ['multipart/form-data', 'image/jpeg', 'image/png']
            },
            auth: 'jwt'
        }
    }
]