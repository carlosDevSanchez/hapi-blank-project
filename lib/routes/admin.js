'use strict';

const Boom = require('boom');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const { Admin } = require('../model');
const { v4: uuidv4 } = require('uuid');
const config = require('../../config');
const utils = require('../utils');

module.exports = [
  {
    path: '/v1/admin',
    method: 'GET',
    options: { 
      handler: async (request, h) => {
        const admin = await Admin.find({});
        return {
          data: admin,
          status: 'SUCCESS'
        }
      },
      description: 'Retorna todos los usuarios',
      tags: ['api', 'get'],
      auth: 'jwt',
    }
  },
  {
    path: '/v1/admin/{id}',
    method: 'GET',
    options: {
      handler: async (request, h) => {
        const { id } = request.params;

        const user = await Admin.findOne({ _id: id });

        if(!user){
          throw Boom.forbidden();
        }

        user.password = '';

        return {
          data: user,
          status: 'SUCCESS'
        }
      },
      description: 'Retorna un usuario a traves de su id',
      tags: ['api'],
      auth: 'jwt'
    }
  },
  {
    path: '/v1/admin',
    method: 'POST',
    options: {
      handler: async (request, h) => {
        const { payload } = request;

        let actual = await Admin.countDocuments();
        actual += 1;
        const code = utils.code_consecutive(actual);

        try{
          const user = new Admin({ ...payload, code });
          await user.save();
          return {
            data: user,
            status: 'SUCCESS'
          }
        }catch(e){
          console.log(e)
          throw Boom.internal();
        }

      },
      description: 'Creacion de usuario nuevo',
      tags: ['api', 'admin'],
      validate: {
        payload: Joi.object({
          name: Joi.string().required(),
          email: Joi.string().required(),
          phone: Joi.string().required(),
          password: Joi.string().required(),
          role: Joi.string().required()
        })
      },
      auth: 'jwt'
    }
  },
  {
    path: '/v1/signin',
    method: 'POST',
    options: {
      handler: async (req, h) => {
        const { username, password } = req.payload;

        utils.create_user_admin()

        let admin = await Admin.findOne({ $or: [{ email: { $regex: username, $options:'i' } }, { phone: { $regex: username, $options:'i' } }]});

        if(!admin){
          throw Boom.badRequest('Your login credentials mismatch.');
        }

        const compare = await utils.compare_password(password, admin.password);

        if(!compare){
          throw Boom.badRequest('Your login credentials mismatch.');
        }
        
        admin.password = '';

        const token = jwt.sign({ id: admin._id }, config.KEY, {
          algorithm: 'HS256',
          expiresIn: '24h',
        });
  
        return {
          data: admin,
          token: token
        }
      },
      description: 'Autenticacion de usuarios (Login)',
      tags: ['api', 'authenticate'],
      validate: {
        payload: Joi.object({
          username: Joi.string().required(),
          password: Joi.string().required()
        })
      },
      auth: false
    }
  },
  {
    path: '/v1/forgot',
    method: 'POST',
    options: {
      handler: async (request, h) => {
        const { payload: { email } } = request;

        const user = await Admin.findOne({ email: { $regex: email, $options:'i' } });

        if(!user){
          throw Boom.badGateway('Email address not found. Please contact your supervisor.');
        }

        const code = uuidv4();
        await Admin.findByIdAndUpdate(user._id, { forgotCode: code });

        //send_email(`info@estebansplace.com`, `${user.email}`, user.name, 'Forgot Password', `We're sending you this email because you request a password reset. Click on the link to create new password: <br><a href="${'https://app.estebansplace.com/auth/forgot/'+code}">Reset Password</a>`);

        return {
          status: 'SUCCESS'
        }
      },
      description: 'Envio de correo para reestablecer contraseña',
      tags: ['api', 'forgot'],
      validate: {
        payload: Joi.object({
          email: Joi.string().required()
        })
      },
      auth: false
    }
  },
  {
    path: '/v1/reset-password/{id}',
    method: 'POST',
    options: {
      handler: async (request, h) => {
        const { payload, params: { id } } = request;

        const user = await Admin.findOne({ forgotCode: id });

        if(!user){
          Boom.badGateway('Link at expirated.');
        }

        const update = await Admin.findByIdAndUpdate(user._id, { password: payload.password, forgotCode: '' });

        return {
          data: update,
          status: 'SUCCESS'
        }

      },
      description: 'Modificar contraseña',
      tags: ['api'],
      validate: {
        payload: Joi.object({
          password: Joi.string().required()
        })
      },
      auth: false
    }
  },
  {
    method: 'PUT',
    path: '/v1/admin/{id}',
    options: {
      handler: async (request, h) => {
        const { params: { id }, payload } = request;
        const { name, email, phone } = payload;
          
        const data = payload.password === '' ? 
          { name, email, phone } : { ...payload };

        const update = await Admin.findByIdAndUpdate(id, { ...data });

        return {
          data: update,
          status: 'SUCCESS'
        }
      },
      description: 'Actualizacion de datos de un usuario',
      tags: ['api', 'update'],
      validate: {
        payload: Joi.object({
          name: Joi.string().required(),
          email: Joi.string().required(),
          phone: Joi.string().required(),
          password: Joi.string().allow('').allow(null)
        })
      },
      auth: 'jwt' 
    }
  },
  {
    method: 'DELETE',
    path: '/v1/admin/{id}',
    options: {
      handler: async (request, h) => {
        const { auth: { credentials }, params: { id } } = request;

        if(!credentials.role === 'ADMIN'){
          throw Boom.badRequest('Not permisson.');
        }

        const remove = await Admin.findByIdAndRemove(id);

        return {
          data: remove,
          status: 'SUCCESS'
        }
      },
      description: 'Eliminar un usuario',
      tags: ['api', 'delete'],
      auth: 'jwt'
    }
  }
];
