const Boom = require('boom');
const Sanitize = require('sanitize-filename');
const { v4: uuidv4 } = require('uuid');
const Fs = require('fs');
const Path = require('path');
const Promise = require('bluebird');
const Os = require('os');
const Mime = require('mime-types');
const AWS = require('aws-sdk');

const S3 = new AWS.S3({
    credentials: {
        secretAccessKey: '',
        accessKeyId: ''
    },
    params: {
      Bucket: '',
      ACL: 'public-read'
    }
});

module.exports = uploadS3 = async (file) => {
    try{
        const filename = Sanitize(
            `${uuidv4()}_${file.hapi.filename.replace(/^.*[\\\/]/, '')}`
        );

        const path = Path.resolve(Os.tmpdir(), filename);
        const streamW = Fs.createWriteStream(path);

        await fileHandler(file, streamW);
        const streamR = Fs.createReadStream(path);
        let mimeType = Mime.lookup(filename);
        
        if (!mimeType) {
            mimeType = 'image/jpeg';
        }

        const data = await Promise.fromCallback(function (callback) {
            S3.upload(
            {
                Key: filename,
                Body: streamR,
                ContentType: mimeType
            },
            callback
            );
        });
        const stats = Fs.statSync(path);
        Fs.unlinkSync(path);

        return {
            location: data.Location,
            filename,
            mimeType,
            size: stats.size
        }
    }catch(e){
        console.log(e);
        throw Boom.forbidden();
    }
}

const fileHandler = function (file, stream) {
    return new Promise((resolve, reject) => {
      file.pipe(stream);
      file.on('error', (err) => reject(err));
      file.on('end', (err) => {
        if (err) {
          return reject(err);
        }
  
        resolve();
      });
    });
};