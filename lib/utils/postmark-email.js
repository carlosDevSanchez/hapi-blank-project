const Boom = require('boom');
const postmark = require("postmark");
const serverToken = "";
let client = null//new postmark.ServerClient(serverToken);

const send_email = (from, to, name, subject, message, file) => {
    try{
        client.sendEmail({
            "From": from,
            "To": to,
            "Subject": subject,
            "HtmlBody": `<html><body><h1>Hi ${name}.</h1><br><p>${message}</p><br><a href="${file}">${file}</a></body></html>`
        });
    }catch(e){
        console.log(e)
        throw Boom.forbidden();
    }
}

const send_membership_email = (from, to, name, subject, message, link) => {
    try{
        client.sendEmail({
            "From": from,
            "To": to,
            "Subject": subject,
            "HtmlBody": 
                `<html>
                    <body>
                        <h1>Hi ${name}.</h1><br>
                        <p>${message}</p><br>
                        <a href="${link}">Membership link download</a>
                    </body>
                </html>`
        });
    }catch(e){
        console.log(e)
        throw Boom.forbidden();
    }
}

module.exports = { send_email, send_membership_email}