const bcryptjs = require('bcryptjs');
const { Admin } = require('../model');
const https = require('https');

const hash_password = async (password) => {
    const hash = await bcryptjs.hash(password, 12);
    return hash;
}

const compare_password = async (password, hash) => {
    const result = await bcryptjs.compare(password, hash);
    return result;
}

const create_user_admin = async () => {
    const user = {
        email: 'admin@api.com',
        password: '12345',
        phone: '0987654321',
        firts_name: 'Admin',
        last_name: 'Admin',
    }
    const prev = await Admin.find({ email: 'admin@api.com' });
    if(prev.length)return;

    const admin = new Admin(user);
    await admin.save();
}

const generate_numbers = (max, min) => {
    return Math.floor((Math.random() * (max - min + 1)) + min);
}


const code_consecutive = (actual) => {
    const code = actual + "";
    let format = '';
    for(let i = code.length; i < 4; i++){
        format += '0';
    }
    return format + code;
}

const getSizeUrl = url => {
    return new Promise((res, rej) => {
        let req = https.get(url);
        req.once("response", r => {
            req.destroy();
            let c = parseInt(r.headers['content-length']);
            if(!isNaN(c)) res(c);
            else rej("Couldn't get file size");
        });
        req.once("error", () => rej("Connection error"));
    });
};

module.exports = { hash_password, compare_password, create_user_admin, code_consecutive, getSizeUrl };